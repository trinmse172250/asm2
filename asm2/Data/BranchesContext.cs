﻿using asm2.Models;
using Microsoft.EntityFrameworkCore;


namespace asm2.Data
{
    public class BranchesContext : DbContext
    {
        public BranchesContext(DbContextOptions<BranchesContext> options)
            : base(options)
        {
        }

        public DbSet<Branch> Branches { get; set; }
    }
}
