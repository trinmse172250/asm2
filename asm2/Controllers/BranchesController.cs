﻿using asm2.Data;
using asm2.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace asm2.Controllers
{
    public class BranchesController : Controller
    {
        private readonly BranchesContext db;

        public BranchesController(BranchesContext context)
        {
            this.db = context;
        }

        public IActionResult Index()
        {
            var branches = db.Branches.ToList();
            return View(branches);
        }

        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var branch = db.Branches.FirstOrDefault(m => m.BranchId == id);

            if (branch == null)
            {
                return NotFound();
            }

            return View(branch);
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(Branch branch)
        {
            branch.State = "Active";
            db.Add(branch);
            db.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var branch = db.Branches.Find(id);

            if (branch == null)
            {
                return NotFound();
            }

            return View(branch);
        }

        [HttpPost]
        public IActionResult Edit(int? id, Branch branch)
        {
            if (id != branch.BranchId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                db.Update(branch);
                db.SaveChanges();

                return RedirectToAction(nameof(Index));
            }

            return View(branch);
        }

        public IActionResult Remove(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var branch = db.Branches.FirstOrDefault(m => m.BranchId == id);

            if (branch == null)
            {
                return NotFound();
            }

            return View(branch);
        }

        [HttpPost]
        public IActionResult Remove(int? id, Branch branch)
        {
            if (id != branch.BranchId)
            {
                return NotFound();
            }

            db.Remove(branch);
            db.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
    }
}
