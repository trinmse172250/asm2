# asm2
This is source code for assigment 2.
## Configuration
Change ```ConnectionStrings``` in appsettings.json to your database.

Create branches table using ```Add-Migration InitialCreate``` and ```Update-Database``` in Package Manager Console.

SQL command to add demo data.
```
INSERT INTO Branches (Name, Address, City, State, ZipCode)
VALUES
  (Branch 1', '123 Main St', 'City 1', 'State 1', '12345'),
  (Branch 2', '456 Elm St', 'City 2', 'State 2', '67890'),
  (Branch 3', '789 Oak St', 'City 3', 'State 3', '23456'),
  (Branch 4', '101 Pine St', 'City 4', 'State 4', '78901'),
  (Branch 5', '202 Maple St', 'City 5', 'State 5', '34567'),
  (Branch 6', '303 Cedar St', 'City 6', 'State 6', '89012'),
  (Branch 7', '404 Birch St', 'City 7', 'State 7', '45678'),
  (Branch 8', '505 Spruce St', 'City 8', 'State 8', '90123'),
  (Branch 9', '606 Fir St', 'City 9', 'State 9', '56789'),
  (Branch 10', '707 Redwood St', 'City 10', 'State 10', '11223');
```
## Result
![alt text](https://gitlab.com/trinmse172250/asm2/-/raw/main/Result.png)